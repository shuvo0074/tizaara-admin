import ApiService from "@/core/services/api.service";

// action types
export const CATEGORY_LIST = "category/list";
export const CATEGORY_CREATE = "category/add";
export const CATEGORY_UPDATE = "category/update";
export const CATEGORY_DELETE = "category/delete";
export const RESET_CATEGORY_ERROR = "category/error/reset";

// mutation types
export const SET_CATEGORY_LIST = "setCategoryList";
export const CATEGORY_ADD = "addNewCategory";
export const CATEGORY_MODIFY = "modifyCategory";
export const CATEGORY_REMOVE = "deleteCategory";
export const SET_ERROR = "setError";
export const CATEGORY_SET_LOADING = "categorySetLoading";

const state = {
    errors: null,
    loading: false,
    categories: {},
};

const getters = {
    categoryList(state) {
        return state.categories;
    },
    getCategoryById:state=>id=>state.categories.find(value=>value.id===id),
    categoryErrors(state) {
        return state.errors;
    },
    categoryIsLoaded(state) {
        return !state.loading;
    }
};

const mutations = {
    [SET_ERROR](state, error) {
        state.errors = error;
        state.loading = false;
    },
    [SET_CATEGORY_LIST](state, categories) {
        state.categories = categories;
    },
    [CATEGORY_REMOVE](state, categoryId) {
        let index = state.categories.findIndex(value => value.id === categoryId);
        state.categories.splice(index, 1);
        state.loading = false;
    },
    [CATEGORY_MODIFY](state, category) {
        let index = state.categories.findIndex(value => value.id === category.id)
        state.categories[index] = category;
        state.loading = false;
    },
    [CATEGORY_ADD](state, category) {
        state.categories.push(category);
    },
    [CATEGORY_SET_LOADING](state, loading = true) {
        state.loading = loading;
    }
};

const actions = {
    [CATEGORY_LIST]({commit, dispatch}) {
        commit(CATEGORY_SET_LOADING)
        ApiService.setHeader();
        ApiService.get("category/list")
            .then(({data}) => {
                commit(SET_CATEGORY_LIST, data.result);
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [CATEGORY_CREATE]({commit, dispatch}, category) {
        commit(CATEGORY_SET_LOADING)
        ApiService.setHeader();
        ApiService.post("category", category)
            .then(({data}) => {
                commit(CATEGORY_ADD, data.result);
                commit(SET_ERROR, null);
                dispatch("setModalStatus", false)
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [CATEGORY_UPDATE]({commit, dispatch}, category) {
        commit(CATEGORY_SET_LOADING)
        ApiService.setHeader();
        ApiService.put(`category/${category.get('id')}`, category)
            .then(({data}) => {
                commit(CATEGORY_MODIFY, data.result);
                commit(SET_ERROR, null);
                dispatch("setModalStatus", false)
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [CATEGORY_DELETE]({commit, dispatch}, category) {
        commit(CATEGORY_SET_LOADING)
        ApiService.setHeader();
        ApiService.delete(`category/${category.id}`)
            .then(({data}) => {
                commit(CATEGORY_REMOVE, category.id);
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [RESET_CATEGORY_ERROR]({commit}) {
        commit(SET_ERROR, null)
    }
};


export default {
    state,
    actions,
    mutations,
    getters
};
