import ApiService from "@/core/services/api.service";

// action types
export const DIVISION_LIST = "division/list";
export const DIVISION_CREATE = "division/add";
export const DIVISION_UPDATE = "division/update";
export const DIVISION_DELETE = "division/delete";
export const RESET_DIVISION_ERROR = "division/error/reset";

// mutation types
export const SET_DIVISION_LIST = "setDivisionList";
export const DIVISION_ADD = "addNewDivision";
export const DIVISION_MODIFY = "modifyDivision";
export const DIVISION_REMOVE = "deleteDivision";
export const SET_ERROR = "setError";
export const DIVISION_SET_LOADING = "divisionSetLoading";

const state = {
    errors: null,
    loading: false,
    divisions: {}
};

const getters = {
    divisionList(state) {
        return state.divisions;
    },
    getDivisionName:state=>id=>state.divisions.find(value=>value.id===id),
    divisionErrors(state) {
        return state.errors;
    },
    divisionIsLoaded(state) {
        return !state.loading;
    }
};

const mutations = {
    [SET_ERROR](state, error) {
        state.errors = error;
        state.loading = false;
    },
    [SET_DIVISION_LIST](state, divisions) {
        state.divisions = divisions;
    },
    [DIVISION_REMOVE](state, divisionId) {
        let index = state.divisions.findIndex(value => value.id === divisionId);
        state.divisions.splice(index, 1);
        state.loading = false;
    },
    [DIVISION_MODIFY](state, division) {
        let index = state.divisions.findIndex(value => value.id === division.id)
        state.divisions[index] = division;
        state.loading = false;
    },
    [DIVISION_ADD](state, division) {
        state.divisions.push(division);
    },
    [DIVISION_SET_LOADING](state, loading = true) {
        state.loading = loading;
    }
};

const actions = {
    [DIVISION_LIST]({commit}) {
        commit(DIVISION_SET_LOADING)
        ApiService.setHeader();
        ApiService.get("division")
            .then(({data}) => {
                commit(SET_DIVISION_LIST, data.result);
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [DIVISION_CREATE]({commit, dispatch}, division) {
        commit(DIVISION_SET_LOADING)
        ApiService.setHeader();
        ApiService.post("division", division)
            .then(({data}) => {
                commit(DIVISION_ADD, data.result);
                commit(SET_ERROR, null);
                dispatch("setModalStatus", false)
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [DIVISION_UPDATE]({commit, dispatch}, division) {
        commit(DIVISION_SET_LOADING)
        ApiService.setHeader();
        ApiService.put(`division/${division.id}`, division)
            .then(({data}) => {
                commit(DIVISION_MODIFY, data.result);
                commit(SET_ERROR, null);
                dispatch("setModalStatus", false)
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [DIVISION_DELETE]({commit}, division) {
        commit(DIVISION_SET_LOADING)
        ApiService.setHeader();
        ApiService.delete(`division/${division.id}`)
            .then(({data}) => {
                commit(DIVISION_REMOVE, division.id);
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [RESET_DIVISION_ERROR]({commit}) {
        commit(SET_ERROR, null)
    }
};


export default {
    state,
    actions,
    mutations,
    getters
};
