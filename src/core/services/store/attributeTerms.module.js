import ApiService from "@/core/services/api.service";

// action types
export const ATTRIBUTE_TERM_LIST = "attributeTerm/list";
export const ATTRIBUTE_TERM_CREATE = "attributeTerm/add";
export const ATTRIBUTE_TERM_UPDATE = "attributeTerm/update";
export const ATTRIBUTE_TERM_DELETE = "attributeTerm/delete";
export const RESET_ATTRIBUTE_TERM_ERROR = "attributeTerm/error/reset";

// mutation types
export const SET_ATTRIBUTE_TERM_LIST = "setAttributeTermList";
export const ATTRIBUTE_TERM_ADD = "addNewAttributeTerm";
export const ATTRIBUTE_TERM_MODIFY = "modifyAttributeTerm";
export const ATTRIBUTE_TERM_REMOVE = "deleteAttributeTerm";
export const SET_ERROR = "setError";
export const ATTRIBUTE_TERM_SET_LOADING = "attributeTermSetLoading";

const state = {
    errors: null,
    loading: false,
    attributeTerms: {},
};

const getters = {
    attributeTermList(state) {
        return state.attributeTerms;
    },
    attributeTermErrors(state) {
        return state.errors;
    },
    attributeTermIsLoaded(state) {
        return !state.loading;
    }
};

const mutations = {
    [SET_ERROR](state, error) {
        state.errors = error;
        state.loading = false;
    },
    [SET_ATTRIBUTE_TERM_LIST](state, attributeTerms) {
        state.attributeTerms = attributeTerms;
    },
    [ATTRIBUTE_TERM_REMOVE](state, attributeTermId) {
        let index = state.attributeTerms.findIndex(value => value.id === attributeTermId);
        state.attributeTerms.splice(index, 1);
        state.loading = false;
    },
    [ATTRIBUTE_TERM_MODIFY](state, attributeTerm) {
        let index = state.attributeTerms.findIndex(value => value.id === attributeTerm.id)
        state.attributeTerms[index] = attributeTerm;
        state.loading = false;
    },
    [ATTRIBUTE_TERM_ADD](state, attributeTerm) {
        state.attributeTerms.push(attributeTerm);
    },
    [ATTRIBUTE_TERM_SET_LOADING](state, loading = true) {
        state.loading = loading;
    }
};

const actions = {
    [ATTRIBUTE_TERM_LIST]({commit, dispatch}) {
        commit(ATTRIBUTE_TERM_SET_LOADING)
        ApiService.setHeader();
        ApiService.get("product/attribute_terms")
            .then(({data}) => {
                commit(SET_ATTRIBUTE_TERM_LIST, data.result);
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [ATTRIBUTE_TERM_CREATE]({commit, dispatch}, attributeTerm) {
        commit(ATTRIBUTE_TERM_SET_LOADING)
        ApiService.setHeader();
        ApiService.post("product/attribute_terms", attributeTerm)
            .then(({data}) => {
                commit(ATTRIBUTE_TERM_ADD, data.result);
                commit(SET_ERROR, null);
                dispatch("setModalStatus", false)
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [ATTRIBUTE_TERM_UPDATE]({commit, dispatch}, attributeTerm) {
        commit(ATTRIBUTE_TERM_SET_LOADING)
        ApiService.setHeader();
        ApiService.put(`product/attribute_terms/${attributeTerm.get('id')}`, attributeTerm)
            .then(({data}) => {
                commit(ATTRIBUTE_TERM_MODIFY, data.result);
                commit(SET_ERROR, null);
                dispatch("setModalStatus", false)
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [ATTRIBUTE_TERM_DELETE]({commit, dispatch}, attributeTerm) {
        commit(ATTRIBUTE_TERM_SET_LOADING)
        ApiService.setHeader();
        ApiService.delete(`product/attribute_terms/${attributeTerm.id}`)
            .then(({data}) => {
                commit(ATTRIBUTE_TERM_REMOVE, attributeTerm.id);
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [RESET_ATTRIBUTE_TERM_ERROR]({commit}) {
        commit(SET_ERROR, null)
    }
};


export default {
    state,
    actions,
    mutations,
    getters
};
