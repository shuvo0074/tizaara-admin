import ApiService from "@/core/services/api.service";

// action types
export const ZONE_LIST = "zone/list";
export const ZONE_CREATE = "zone/add";
export const ZONE_UPDATE = "zone/update";
export const ZONE_DELETE = "zone/delete";
export const RESET_ZONE_ERROR = "zone/error/reset";

// mutation types
export const SET_ZONE_LIST = "setZoneList";
export const ZONE_ADD = "addNewZone";
export const ZONE_MODIFY = "modifyZone";
export const ZONE_REMOVE = "deleteZone";
export const SET_ERROR = "setError";
export const ZONE_SET_LOADING = "zoneSetLoading";

const state = {
    errors: null,
    loading: false,
    zones: {}
};

const getters = {
    zoneList(state) {
        return state.zones;
    },
    zoneErrors(state) {
        return state.errors;
    },
    zoneIsLoaded(state) {
        return !state.loading;
    }
};

const mutations = {
    [SET_ERROR](state, error) {
        state.errors = error;
        state.loading = false;
    },
    [SET_ZONE_LIST](state, zones) {
        state.zones = zones;
    },
    [ZONE_REMOVE](state, zoneId) {
        let index = state.zones.findIndex(value => value.id === zoneId);
        state.zones.splice(index, 1);
        state.loading = false;
    },
    [ZONE_MODIFY](state, zone) {
        let index = state.zones.findIndex(value => value.id === zone.id)
        state.zones[index] = zone;
        state.loading = false;
    },
    [ZONE_ADD](state, zone) {
        state.zones.push(zone);
    },
    [ZONE_SET_LOADING](state, loading = true) {
        state.loading = loading;
    }
};

const actions = {
    [ZONE_LIST]({commit}) {
        commit(ZONE_SET_LOADING)
        ApiService.setHeader();
        ApiService.get("area")
            .then(({data}) => {
                commit(SET_ZONE_LIST, data.result);
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [ZONE_CREATE]({commit, dispatch}, zone) {
        commit(ZONE_SET_LOADING)
        ApiService.setHeader();
        ApiService.post("area", zone)
            .then(({data}) => {
                commit(ZONE_ADD, data.result);
                commit(SET_ERROR, null);
                dispatch("setModalStatus", false)
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [ZONE_UPDATE]({commit, dispatch}, zone) {
        commit(ZONE_SET_LOADING)
        ApiService.setHeader();
        ApiService.put(`area/${zone.id}`, zone)
            .then(({data}) => {
                commit(ZONE_MODIFY, data.result);
                commit(SET_ERROR, null);
                dispatch("setModalStatus", false)
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [ZONE_DELETE]({commit}, zone) {
        commit(ZONE_SET_LOADING)
        ApiService.setHeader();
        ApiService.delete(`area/${zone.id}`)
            .then(({data}) => {
                commit(ZONE_REMOVE, zone.id);
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [RESET_ZONE_ERROR]({commit}) {
        commit(SET_ERROR, null)
    }
};


export default {
    state,
    actions,
    mutations,
    getters
};
