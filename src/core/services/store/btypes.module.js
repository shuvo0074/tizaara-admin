import ApiService from "@/core/services/api.service";

// action types
export const BTYPE_LIST = "btype/list";
export const BTYPE_CREATE = "btype/add";
export const BTYPE_UPDATE = "btype/update";
export const BTYPE_DELETE = "btype/delete";
export const RESET_BTYPE_ERROR = "btype/error/reset";

// mutation types
export const SET_BTYPE_LIST = "setBtypeList";
export const BTYPE_ADD = "addNewBtype";
export const BTYPE_MODIFY = "modifyBtype";
export const BTYPE_REMOVE = "deleteBtype";
export const SET_ERROR = "setError";
export const BTYPE_SET_LOADING = "btypeSetLoading";

const state = {
    errors: null,
    loading: false,
    btypes: {},
};

const getters = {
    btypeList(state) {
        return state.btypes;
    },
    btypeErrors(state) {
        return state.errors;
    },
    btypeIsLoaded(state) {
        return !state.loading;
    }
};

const mutations = {
    [SET_ERROR](state, error) {
        state.errors = error;
        state.loading = false;
    },
    [SET_BTYPE_LIST](state, btypes) {
        state.btypes = btypes;
    },
    [BTYPE_REMOVE](state, btypeId) {
        let index = state.btypes.findIndex(value => value.id === btypeId);
        state.btypes.splice(index, 1);
        state.loading = false;
    },
    [BTYPE_MODIFY](state, btype) {
        let index = state.btypes.findIndex(value => value.id === btype.id)
        state.btypes[index] = btype;
        state.loading = false;
    },
    [BTYPE_ADD](state, btype) {
        state.btypes.push(btype);
    },
    [BTYPE_SET_LOADING](state, loading = true) {
        state.loading = loading;
    }
};

const actions = {
    [BTYPE_LIST]({commit, dispatch}) {
        commit(BTYPE_SET_LOADING)
        ApiService.setHeader();
        ApiService.get("btype")
            .then(({data}) => {
                commit(SET_BTYPE_LIST, data.result);
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [BTYPE_CREATE]({commit, dispatch}, btype) {
        commit(BTYPE_SET_LOADING)
        ApiService.setHeader();
        ApiService.post("btype", btype)
            .then(({data}) => {
                commit(BTYPE_ADD, data.result);
                commit(SET_ERROR, null);
                dispatch("setModalStatus", false)
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [BTYPE_UPDATE]({commit, dispatch}, btype) {
        commit(BTYPE_SET_LOADING)
        ApiService.setHeader();
        ApiService.put(`btype/${btype.get('id')}`, btype)
            .then(({data}) => {
                commit(BTYPE_MODIFY, data.result);
                commit(SET_ERROR, null);
                dispatch("setModalStatus", false)
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [BTYPE_DELETE]({commit, dispatch}, btype) {
        commit(BTYPE_SET_LOADING)
        ApiService.setHeader();
        ApiService.delete(`btype/${btype.id}`)
            .then(({data}) => {
                commit(BTYPE_REMOVE, btype.id);
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [RESET_BTYPE_ERROR]({commit}) {
        commit(SET_ERROR, null)
    }
};


export default {
    state,
    actions,
    mutations,
    getters
};
