import Vue from "vue";
import Vuex from "vuex";

import auth from "./auth.module";
import htmlClass from "./htmlclass.module";
import config from "./config.module";
import breadcrumbs from "./breadcrumbs.module";
import brands from "./brands.module";
import countries from "./countries.module";
import zones from "./zones.module";
import categories from "./categories.module";
import btypes from "./btypes.module";
import attributes from "./attributes.module";
import attributeTerms from "./attributeTerms.module";
import divisions from "./divisions.module";
import cities from "./cities.module";
import attributeGroups from "./attributeGroups.module";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        dialog: false
    },
    getters: {
        dialog: state => state.dialog,
    },
    mutations: {
        modalStatus: (state, dialog) => state.dialog = dialog,
    },
    actions: {
        setModalStatus: ({commit}, dialog) => commit('modalStatus', dialog),
    },
    modules: {
        auth,
        htmlClass,
        config,
        breadcrumbs,
        brands,
        countries,
        zones,
        categories,
        btypes,
        attributes,
        attributeTerms,
        divisions,
        cities,
        attributeGroups
    }
});
