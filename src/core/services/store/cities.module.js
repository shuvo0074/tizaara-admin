import ApiService from "@/core/services/api.service";

// action types
export const CITY_LIST = "city/list";
export const CITY_CREATE = "city/add";
export const CITY_UPDATE = "city/update";
export const CITY_DELETE = "city/delete";
export const RESET_CITY_ERROR = "city/error/reset";

// mutation types
export const SET_CITY_LIST = "setCityList";
export const CITY_ADD = "addNewCity";
export const CITY_MODIFY = "modifyCity";
export const CITY_REMOVE = "deleteCity";
export const SET_ERROR = "setError";
export const CITY_SET_LOADING = "citySetLoading";

const state = {
    errors: null,
    loading: false,
    cities: {}
};

const getters = {
    cityList(state) {
        return state.cities;
    },
    getCityName:state=>id=>state.cities.find(value=>value.id===id),
    cityErrors(state) {
        return state.errors;
    },
    cityIsLoaded(state) {
        return !state.loading;
    }
};

const mutations = {
    [SET_ERROR](state, error) {
        state.errors = error;
        state.loading = false;
    },
    [SET_CITY_LIST](state, cities) {
        state.cities = cities;
    },
    [CITY_REMOVE](state, cityId) {
        let index = state.cities.findIndex(value => value.id === cityId);
        state.cities.splice(index, 1);
        state.loading = false;
    },
    [CITY_MODIFY](state, city) {
        let index = state.cities.findIndex(value => value.id === city.id)
        state.cities[index] = city;
        state.loading = false;
    },
    [CITY_ADD](state, city) {
        state.cities.push(city);
    },
    [CITY_SET_LOADING](state, loading = true) {
        state.loading = loading;
    }
};

const actions = {
    [CITY_LIST]({commit}) {
        commit(CITY_SET_LOADING)
        ApiService.setHeader();
        ApiService.get("city")
            .then(({data}) => {
                commit(SET_CITY_LIST, data.result);
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [CITY_CREATE]({commit, dispatch}, city) {
        commit(CITY_SET_LOADING)
        ApiService.setHeader();
        ApiService.post("city", city)
            .then(({data}) => {
                commit(CITY_ADD, data.result);
                commit(SET_ERROR, null);
                dispatch("setModalStatus", false)
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [CITY_UPDATE]({commit, dispatch}, city) {
        commit(CITY_SET_LOADING)
        ApiService.setHeader();
        ApiService.put(`city/${city.id}`, city)
            .then(({data}) => {
                commit(CITY_MODIFY, data.result);
                commit(SET_ERROR, null);
                dispatch("setModalStatus", false)
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [CITY_DELETE]({commit}, city) {
        commit(CITY_SET_LOADING)
        ApiService.setHeader();
        ApiService.delete(`city/${city.id}`)
            .then(({data}) => {
                commit(CITY_REMOVE, city.id);
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [RESET_CITY_ERROR]({commit}) {
        commit(SET_ERROR, null)
    }
};


export default {
    state,
    actions,
    mutations,
    getters
};
