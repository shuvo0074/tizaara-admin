import ApiService from "@/core/services/api.service";

// action types
export const COUNTRY_LIST = "country/list";
export const COUNTRY_CREATE = "country/add";
export const COUNTRY_UPDATE = "country/update";
export const COUNTRY_DELETE = "country/delete";
export const RESET_COUNTRY_ERROR = "country/error/reset";
export const COUNTRY_FORM_STATUS = "country/from/status";

// mutation types
export const SET_COUNTRY_LIST = "setCountryList";
export const COUNTRY_ADD = "addNewCountry";
export const COUNTRY_MODIFY = "modifyCountry";
export const COUNTRY_REMOVE = "deleteCountry";
export const SET_ERROR = "setError";
export const COUNTRY_SET_LOADING = "countrySetLoading";

const state = {
    errors: null,
    loading: false,
    countries: []
};

const getters = {
    countryList(state) {
        return state.countries;
    },
     getCountryById:state=>id=>state.countries.find(value=>value.id===id),
    countryErrors(state) {
        return state.errors;
    },
    countryIsLoaded(state) {
        return !state.loading;
    }
};

const mutations = {
    [SET_ERROR](state, error) {
        state.errors = error;
        state.loading = false;
    },
    [SET_COUNTRY_LIST](state, countries) {
        state.countries = countries;
    },
    [COUNTRY_REMOVE](state, countryId) {
        let index = state.countries.findIndex(value => value.id === countryId);
        state.countries.splice(index, 1);
        state.loading = false;
    },
    [COUNTRY_MODIFY](state, country) {
        let index = state.countries.findIndex(value => value.id === country.id)
        state.countries[index] = country;
        state.loading = false;
    },
    [COUNTRY_ADD](state, country) {
        state.countries.push(country);
    },
    [COUNTRY_SET_LOADING](state, loading = true) {
        state.loading = loading;
    }
};

const actions = {
    [COUNTRY_LIST]({commit}) {
        commit(COUNTRY_SET_LOADING)
        ApiService.setHeader();
        ApiService.get("country")
            .then(({data}) => {
                commit(SET_COUNTRY_LIST, data.result);
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [COUNTRY_CREATE]({commit, dispatch}, country) {
        commit(COUNTRY_SET_LOADING)
        ApiService.setHeader();
        ApiService.post("country", country)
            .then(({data}) => {
                commit(COUNTRY_ADD, data.result);
                commit(SET_ERROR, null);
                dispatch("setModalStatus", false)
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [COUNTRY_UPDATE]({commit, dispatch}, country) {
        commit(COUNTRY_SET_LOADING)
        ApiService.setHeader();
        ApiService.put(`country/${country.id}`, country)
            .then(({data}) => {
                commit(COUNTRY_MODIFY, data.result);
                commit(SET_ERROR, null);
                dispatch("setModalStatus", false)
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [COUNTRY_DELETE]({commit}, country) {
        commit(COUNTRY_SET_LOADING)
        ApiService.setHeader();
        ApiService.delete(`country/${country.id}`)
            .then(({data}) => {
                commit(COUNTRY_REMOVE, country.id);
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [RESET_COUNTRY_ERROR]({commit}) {
        commit(SET_ERROR, null)
    }
};


export default {
    state,
    actions,
    mutations,
    getters
};
