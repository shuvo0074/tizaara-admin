import ApiService from "@/core/services/api.service";

// action types
export const BRAND_LIST = "brand/list";
export const BRAND_CREATE = "brand/add";
export const BRAND_UPDATE = "brand/update";
export const BRAND_DELETE = "brand/delete";
export const RESET_BRAND_ERROR = "brand/error/reset";

// mutation types
export const SET_BRAND_LIST = "setBrandList";
export const BRAND_ADD = "addNewBrand";
export const BRAND_MODIFY = "modifyBrand";
export const BRAND_REMOVE = "deleteBrand";
export const SET_ERROR = "setError";
export const BRAND_SET_LOADING = "brandSetLoading";

const state = {
    errors: null,
    loading: false,
    brands: {},
};

const getters = {
    brandList(state) {
        return state.brands;
    },
    brandErrors(state) {
        return state.errors;
    },
    brandIsLoaded(state) {
        return !state.loading;
    }
};

const mutations = {
    [SET_ERROR](state, error) {
        state.errors = error;
        state.loading = false;
    },
    [SET_BRAND_LIST](state, brands) {
        state.brands = brands;
    },
    [BRAND_REMOVE](state, brandId) {
        let index = state.brands.findIndex(value => value.id === brandId);
        state.brands.splice(index, 1);
        state.loading = false;
    },
    [BRAND_MODIFY](state, brand) {
        let index = state.brands.findIndex(value => value.id === brand.id)
        state.brands[index] = brand;
        state.loading = false;
    },
    [BRAND_ADD](state, brand) {
        state.brands.push(brand);
    },
    [BRAND_SET_LOADING](state, loading = true) {
        state.loading = loading;
    }
};

const actions = {
    [BRAND_LIST]({commit, dispatch}) {
        commit(BRAND_SET_LOADING)
        ApiService.setHeader();
        ApiService.get("brand")
            .then(({data}) => {
                commit(SET_BRAND_LIST, data.result);
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [BRAND_CREATE]({commit, dispatch}, brand) {
        commit(BRAND_SET_LOADING)
        ApiService.setHeader();
        ApiService.post("brand", brand)
            .then(({data}) => {
                commit(BRAND_ADD, data.result);
                commit(SET_ERROR, null);
                dispatch("setModalStatus", false)
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [BRAND_UPDATE]({commit, dispatch}, brand) {
        commit(BRAND_SET_LOADING)
        ApiService.setHeader();
        ApiService.put(`brand/${brand.get('id')}`, brand)
            .then(({data}) => {
                commit(BRAND_MODIFY, data.result);
                commit(SET_ERROR, null);
                dispatch("setModalStatus", false)
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [BRAND_DELETE]({commit, dispatch}, brand) {
        commit(BRAND_SET_LOADING)
        ApiService.setHeader();
        ApiService.delete(`brand/${brand.id}`)
            .then(({data}) => {
                commit(BRAND_REMOVE, brand.id);
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [RESET_BRAND_ERROR]({commit}) {
        commit(SET_ERROR, null)
    }
};


export default {
    state,
    actions,
    mutations,
    getters
};
