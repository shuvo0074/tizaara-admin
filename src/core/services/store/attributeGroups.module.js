import ApiService from "@/core/services/api.service";

// action types
export const ATTRIBUTE_GROUP_LIST = "attributeGroup/list";
export const ATTRIBUTE_GROUP_CREATE = "attributeGroup/add";
export const ATTRIBUTE_GROUP_UPDATE = "attributeGroup/update";
export const ATTRIBUTE_GROUP_DELETE = "attributeGroup/delete";
export const RESET_ATTRIBUTE_GROUP_ERROR = "attributeGroup/error/reset";

// mutation types
export const SET_ATTRIBUTE_GROUP_LIST = "setAttributeGroupList";
export const ATTRIBUTE_GROUP_ADD = "addNewAttributeGroup";
export const ATTRIBUTE_GROUP_MODIFY = "modifyAttributeGroup";
export const ATTRIBUTE_GROUP_REMOVE = "deleteAttributeGroup";
export const SET_ERROR = "setError";
export const ATTRIBUTE_GROUP_SET_LOADING = "attributeGroupSetLoading";

const state = {
    errors: null,
    loading: false,
    attributeGroups: {},
};

const getters = {
    attributeGroupList(state) {
    return state.attributeGroups;
},
attributeGroupErrors(state) {
    return state.errors;
},
attributeGroupIsLoaded(state) {
    return !state.loading;
}
};

const mutations = {
    [SET_ERROR](state, error) {
    state.errors = error;
    state.loading = false;
},
[SET_ATTRIBUTE_GROUP_LIST](state, attributeGroups) {
    state.attributeGroups = attributeGroups;
},
[ATTRIBUTE_GROUP_REMOVE](state, attributeGroupId) {
    let index = state.attributeGroups.findIndex(value => value.id === attributeGroupId);
    state.attributeGroups.splice(index, 1);
    state.loading = false;
},
[ATTRIBUTE_GROUP_MODIFY](state, attributeGroup) {
    let index = state.attributeGroups.findIndex(value => value.id === attributeGroup.id)
    state.attributeGroups[index] = attributeGroup;
    state.loading = false;
},
[ATTRIBUTE_GROUP_ADD](state, attributeGroup) {
    state.attributeGroups.push(attributeGroup);
},
[ATTRIBUTE_GROUP_SET_LOADING](state, loading = true) {
    state.loading = loading;
}
};

const actions = {
    [ATTRIBUTE_GROUP_LIST]({commit, dispatch}) {
    commit(ATTRIBUTE_GROUP_SET_LOADING)
    ApiService.setHeader();
    ApiService.get("product/attribute_groups")
        .then(({data}) => {
        commit(SET_ATTRIBUTE_GROUP_LIST, data.result);
})
.catch(({response}) => {
    commit(SET_ERROR, response.data.errors);
});
},
[ATTRIBUTE_GROUP_CREATE]({commit, dispatch}, attributeGroup) {
    commit(ATTRIBUTE_GROUP_SET_LOADING)
    ApiService.setHeader();
    ApiService.post("product/attribute_groups", attributeGroup)
        .then(({data}) => {
        commit(ATTRIBUTE_GROUP_ADD, data.result);
    commit(SET_ERROR, null);
    dispatch("setModalStatus", false)
})
.catch(({response}) => {
    commit(SET_ERROR, response.data.errors);
});
},
[ATTRIBUTE_GROUP_UPDATE]({commit, dispatch}, attributeGroup) {
    commit(ATTRIBUTE_GROUP_SET_LOADING)
    ApiService.setHeader();
    ApiService.put(`product/attribute_groups/${attributeGroup.get('id')}`, attributeGroup)
.then(({data}) => {
        commit(ATTRIBUTE_GROUP_MODIFY, data.result);
    commit(SET_ERROR, null);
    dispatch("setModalStatus", false)
})
.catch(({response}) => {
    commit(SET_ERROR, response.data.errors);
});
},
[ATTRIBUTE_GROUP_DELETE]({commit, dispatch}, attributeGroup) {
    commit(ATTRIBUTE_GROUP_SET_LOADING)
    ApiService.setHeader();
    ApiService.delete(`product/attribute_groups/${attributeGroup.id}`)
.then(({data}) => {
        commit(ATTRIBUTE_GROUP_REMOVE, attributeGroup.id);
})
.catch(({response}) => {
    commit(SET_ERROR, response.data.errors);
});
},
[RESET_ATTRIBUTE_GROUP_ERROR]({commit}) {
    commit(SET_ERROR, null)
}
};


export default {
    state,
    actions,
    mutations,
    getters
};
