import ApiService from "@/core/services/api.service";

// action types
export const ATTRIBUTE_LIST = "attribute/list";
export const ATTRIBUTE_CREATE = "attribute/add";
export const ATTRIBUTE_UPDATE = "attribute/update";
export const ATTRIBUTE_DELETE = "attribute/delete";
export const RESET_ATTRIBUTE_ERROR = "attribute/error/reset";

// mutation types
export const SET_ATTRIBUTE_LIST = "setAttributeList";
export const ATTRIBUTE_ADD = "addNewAttribute";
export const ATTRIBUTE_MODIFY = "modifyAttribute";
export const ATTRIBUTE_REMOVE = "deleteAttribute";
export const SET_ERROR = "setError";
export const ATTRIBUTE_SET_LOADING = "attributeSetLoading";

const state = {
    errors: null,
    loading: false,
    attributes: {},
};

const getters = {
    attributeList(state) {
        return state.attributes;
    },
    getAttributeName:state=>id=>state.attributes.find(value=>value.id===id),
    attributeErrors(state) {
        return state.errors;
    },
    attributeIsLoaded(state) {
        return !state.loading;
    }
};

const mutations = {
    [SET_ERROR](state, error) {
        state.errors = error;
        state.loading = false;
    },
    [SET_ATTRIBUTE_LIST](state, attributes) {
        state.attributes = attributes;
    },
    [ATTRIBUTE_REMOVE](state, attributeId) {
        let index = state.attributes.findIndex(value => value.id === attributeId);
        state.attributes.splice(index, 1);
        state.loading = false;
    },
    [ATTRIBUTE_MODIFY](state, attribute) {
        let index = state.attributes.findIndex(value => value.id === attribute.id)
        state.attributes[index] = attribute;
        state.loading = false;
    },
    [ATTRIBUTE_ADD](state, attribute) {
        state.attributes.push(attribute);
    },
    [ATTRIBUTE_SET_LOADING](state, loading = true) {
        state.loading = loading;
    }
};

const actions = {
    [ATTRIBUTE_LIST]({commit, dispatch}) {
        commit(ATTRIBUTE_SET_LOADING)
        ApiService.setHeader();
        ApiService.get("product/attribute")
            .then(({data}) => {
                commit(SET_ATTRIBUTE_LIST, data.result);
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [ATTRIBUTE_CREATE]({commit, dispatch}, attribute) {
        commit(ATTRIBUTE_SET_LOADING)
        ApiService.setHeader();
        ApiService.post("product/attribute", attribute)
            .then(({data}) => {
                commit(ATTRIBUTE_ADD, data.result);
                commit(SET_ERROR, null);
                dispatch("setModalStatus", false)
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [ATTRIBUTE_UPDATE]({commit, dispatch}, attribute) {
        commit(ATTRIBUTE_SET_LOADING)
        ApiService.setHeader();
        ApiService.put(`product/attribute/${attribute.get('id')}`, attribute)
            .then(({data}) => {
                commit(ATTRIBUTE_MODIFY, data.result);
                commit(SET_ERROR, null);
                dispatch("setModalStatus", false)
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [ATTRIBUTE_DELETE]({commit, dispatch}, attribute) {
        commit(ATTRIBUTE_SET_LOADING)
        ApiService.setHeader();
        ApiService.delete(`product/attribute/${attribute.id}`)
            .then(({data}) => {
                commit(ATTRIBUTE_REMOVE, attribute.id);
            })
            .catch(({response}) => {
                commit(SET_ERROR, response.data.errors);
            });
    },
    [RESET_ATTRIBUTE_ERROR]({commit}) {
        commit(SET_ERROR, null)
    }
};


export default {
    state,
    actions,
    mutations,
    getters
};
