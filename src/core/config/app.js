export const app_name = process.env.VUE_APP_NAME;
export const front_base_url = process.env.VUE_APP_FRONT_BASE_URL;
export const api_base_url = process.env.VUE_APP_API_BASE_URL;